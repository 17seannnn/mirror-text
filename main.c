#include <stdio.h>
#include <stdlib.h>

char *strglue(int count, char **s)
{
        char *glue;
        int i, j, k, chars;
        chars = 0;
        for (i = 0; i < count; i++)
                for (j = 0; s[i][j]; j++)
                        chars++;
        glue = malloc(sizeof(char)*(chars + count));
        for (i = 0, k = 0; i < count; i++) {
                for (j = 0; s[i][j]; j++)
                        glue[k++] = s[i][j];
                glue[k] = k == chars + count - 1 ? 0 : ' ';
                k++;
        }
        return glue;
}

int mystrlen(const char *s)
{
        const char *t;
        for (t = s; *t; t++)
                {}
        return t - s;
}

int strwrite(char *dst, const char *src, int pos, int size)
{
        for (; *src && pos < size; src++)
                dst[pos++] = *src;
        return pos;
}

void strrev(char *s)
{
        int i, k, tmp;
        for (i = 0, k = mystrlen(s)-1; i < k; i++, k--) {
                tmp = s[i];
                s[i] = s[k];
                s[k] = tmp;
        }
}

char *mirror(char *text)
{
        char *new, *ttext, *tmp;
        int new_size;
        int i, pos = 0, lines = 2, len = 0, maxlen = 0;
        for (ttext = text;; ttext++) {
                if (*ttext == ' ' || !*ttext) {
                        if (maxlen < len)
                                maxlen = len;
                        lines++;
                        len = 0;
                } else {
                        len++;
                }
                if (!*ttext)
                        break;
        }
        new_size = sizeof(char) * (maxlen + 5) * lines;
        new = malloc(new_size);
        tmp = malloc(sizeof(char) * (maxlen + 1));
        for (i = 0; i < maxlen + 4; i++)
                pos = strwrite(new, "*", pos, new_size);
        pos = strwrite(new, "\n", pos, new_size);
        for (i = 0;; text++) {
                if (*text == ' ' || !*text) {
                        tmp[i] = 0;
                        strrev(tmp);
                        pos = strwrite(new, "* ", pos, new_size);
                        pos = strwrite(new, tmp, pos, new_size);
                        for (; i < maxlen; i++)
                                pos = strwrite(new, " ", pos, new_size);
                        pos = strwrite(new, " *\n", pos, new_size);
                        i = 0;
                } else {
                        tmp[i] = *text;
                        i++;
                }
                if (!*text)
                        break;
        }
        for (i = 0; i < maxlen + 4; i++)
                pos = strwrite(new, "*", pos, new_size);
        new[pos] = 0;
        free(tmp);
        return new;
}

int main(int argc, char **argv)
{
        char *glue, *s;
        if (argc < 2) {
                fprintf(stderr, "No params given\n");
                return 1;
        }
        glue = strglue(argc-1, ++argv);
        s = mirror(glue);
        printf("%s\n", s);
        free(glue);
        free(s);
        return 0;
}
