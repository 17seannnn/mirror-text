# Mirror Text

## Install
```bash
git clone https://github.com/17sean/mirror-text.git
cd mirror-text
gcc main.c -o mirror
```

## Examples
```bash
% ./mirror Very Interesting Text
***************
* yreV        *
* gnitseretnI *
* txeT        *
***************
```
```bash
% ./mirror hello
*********
* olleh *
*********
```
